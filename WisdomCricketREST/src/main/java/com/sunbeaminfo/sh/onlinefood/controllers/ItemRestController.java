package com.sunbeaminfo.sh.onlinefood.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeaminfo.sh.onlinefood.entities.Item;
import com.sunbeaminfo.sh.onlinefood.services.ItemService;

@RestController
public class ItemRestController {
	@Autowired
	private ItemService itemService;
	
	@CrossOrigin
	@GetMapping(value="/jtable/items/{id}")
	public Item getItemById(@PathVariable("id") int id) {
		return itemService.getItem(id);
	}
	
	@CrossOrigin
	@PostMapping(value="/jtable/items")
	public Map<String, Object> getItems() {
		Map<String, Object> res = new HashMap<String, Object>();
		try {
			List<Item> list = itemService.getItems();
			res.put("Result", "OK");
			res.put("Records", list);
		}catch (Exception e) {
			res.put("Result", "ERROR");
			res.put("Message", e.getMessage());
		}
		return res;
	}	
	
	@PostMapping(value="/jtable/newitem")
	public Map<String, Object> newItem(Item i) {
		Map<String, Object> res = new HashMap<String, Object>();
		try {
			itemService.saveItem(i);
			res.put("Result", "OK");
			res.put("Record", i);
		}catch (Exception e) {
			res.put("Result", "ERROR");
			res.put("Message", e.getMessage());
		}
		return res;
	}
	
	@PostMapping(value="/jtable/edititem")
	public Map<String, Object> editDept(Item i) {
		Map<String, Object> res = new HashMap<String, Object>();
		try {
			itemService.updateItem(i);
			res.put("Result", "OK");
		}catch (Exception e) {
			res.put("Result", "ERROR");
			res.put("Message", e.getMessage());
		}
		return res;
	}
	
	@PostMapping(value="/jtable/delitem")
	public Map<String, Object> delItem(@RequestParam("id") int itemId) {
		Map<String, Object> res = new HashMap<String, Object>();
		try {
			itemService.deleteById(itemId);
			res.put("Result", "OK");
		}catch (Exception e) {
			e.printStackTrace();
			res.put("Result", "ERROR");
			res.put("Message", e.getMessage());
		}
		return res;
	}
}
