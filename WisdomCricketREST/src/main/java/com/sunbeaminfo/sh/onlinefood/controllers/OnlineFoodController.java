package com.sunbeaminfo.sh.onlinefood.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OnlineFoodController {
	@RequestMapping("/default")
	public String default1() {
		return "default";
	}
	@RequestMapping("/index")
	public String index() {
		return "index";
	}
}
