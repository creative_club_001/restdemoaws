package com.sunbeaminfo.sh.onlinefood.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="CUSTOMERS")
public class Customer implements Serializable {
	private static final long serialVersionUID = 1L;
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	@Column
	private int id;
	
	@NotBlank
	@Column
	private String name;
	
	@JsonIgnore
	@NotBlank
	@Length(min=4)
	@Column
	private String password;
	
	@NotBlank
	@Email
	@Column
	private String email;
	
	@JsonProperty("phone")
	@NotBlank
	@Pattern(regexp="^[0-9]{10}$")
	@Column
	private String mobile;
	
	@NotBlank
	@Length(min=6)
	@Column
	private String address;
	
	public Customer() {
	}
	public Customer(int id, String name, String password, String email, String mobile, String address) {		this.id = id;
		this.name = name;
		this.password = password;
		this.email = email;
		this.mobile = mobile;
		this.address = address;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void copyFrom(Customer other) {
		this.id = other.id;
		this.name = other.name;
		this.password = other.password;
		this.email = other.email;
		this.mobile = other.mobile;
		this.address = other.address;		
	}
	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", password=" + password + ", email=" + email + ", mobile="
				+ mobile + ", address=" + address + "]";
	}
}
