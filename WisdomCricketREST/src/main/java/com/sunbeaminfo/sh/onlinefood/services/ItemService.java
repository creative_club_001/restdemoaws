package com.sunbeaminfo.sh.onlinefood.services;

import java.util.List;

import com.sunbeaminfo.sh.onlinefood.entities.Item;

public interface ItemService {

	Item getItem(int id);

	List<Item> getItems(String category);

	List<String> getCategories();

	List<Item> getItems();

	void saveItem(Item i);

	void updateItem(Item i);

	void deleteById(int itemId);
}
