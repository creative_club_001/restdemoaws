package com.sunbeaminfo.sh.onlinefood.daos;

import com.sunbeaminfo.sh.onlinefood.entities.Customer;

public interface CustomerDao {

	Customer getCustomerByEmail(String email);

	Customer getCustomerByMobile(String mobile);

	Customer getCustomerById(int id);

	void saveCustomer(Customer cust);
}