package com.sunbeaminfo.sh.onlinefood.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeaminfo.sh.onlinefood.entities.Customer;
import com.sunbeaminfo.sh.onlinefood.services.CustomerService;

@RestController
public class CustomerRestController {
	@Autowired
	private CustomerService custService;
	
	@GetMapping(value="/customers/{id}")
	public Customer getCustomer(@PathVariable("id") int id) {
		Customer cust = custService.getCustomerById(id);
		return cust;
	}
	
	@PostMapping("/customers")
	public Customer registerCustomer(@RequestBody Customer cust) {
		try {
			custService.saveCustomer(cust);
			return cust;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}	
	
	@GetMapping("/customers2/{id}")
	public ResponseEntity<Customer> getCustomer2(@PathVariable("id") int id) {
		Customer cust = custService.getCustomerById(id);
		if(cust != null)
			return new ResponseEntity<Customer>(cust, HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@PostMapping("/customers2")
	public ResponseEntity<Customer> registerCustomer2(@RequestBody Customer cust) {
		try {
			custService.saveCustomer(cust);
			return new ResponseEntity<Customer>(cust, HttpStatus.OK);
		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
