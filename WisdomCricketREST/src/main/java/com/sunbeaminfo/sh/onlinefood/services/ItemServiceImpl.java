package com.sunbeaminfo.sh.onlinefood.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeaminfo.sh.onlinefood.daos.ItemDao;
import com.sunbeaminfo.sh.onlinefood.entities.Item;

@Service
public class ItemServiceImpl implements ItemService {
	@Autowired
	private ItemDao itemDao;
	
	@Transactional
	public List<String> getCategories() {
		return itemDao.getCategories();
	}
	
	@Transactional
	public List<Item> getItems(String category) {
		return itemDao.getItems(category);
	}
	
	@Transactional
	public Item getItem(int id) {
		return itemDao.getItem(id);
	}

	@Transactional
	public List<Item> getItems() {
		return itemDao.getItems();
	}
	
	@Transactional
	public void saveItem(Item i) {
		itemDao.saveItem(i);
	}
	
	@Transactional
	public void updateItem(Item i) {
		itemDao.updateItem(i);
	}
	
	@Transactional
	public void deleteById(int itemId) {
		itemDao.deleteById(itemId);
	}
}
