package com.sunbeaminfo.sh.onlinefood.daos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sunbeaminfo.sh.onlinefood.entities.Item;

@Repository
public class ItemDaoImpl implements ItemDao {
	@Autowired
	private SessionFactory factory;
	@Override
	public List<String> getCategories() {
		Session session = factory.getCurrentSession();
		Query<String> q = session.getNamedQuery("qryCategories");
		return q.getResultList();
	}
	@Override
	public List<Item> getItems(String category) {
		Session session = factory.getCurrentSession();
		Query<Item> q = session.getNamedQuery("qryCategoryItems");
		q.setParameter("p_category", category);
		return q.getResultList();
	}
	@Override
	public Item getItem(int id) {
		Session session = factory.getCurrentSession();
		return session.get(Item.class, id);
	}
	@Override
	public List<Item> getItems() {
		Session session = factory.getCurrentSession();
		Query<Item> q = session.getNamedQuery("qryItems");
		return q.getResultList();
	}
	
	@Override
	public void saveItem(Item i) {
		Session session = factory.getCurrentSession();
		session.persist(i);
	}
	
	@Override
	public void updateItem(Item i) {
		Session session = factory.getCurrentSession();
		session.merge(i);		
	}
	
	@Override
	public void deleteById(int itemId) {
		Session session = factory.getCurrentSession();
		Item i = session.get(Item.class, itemId);
		if(i != null)
			session.delete(i);
	}
}

