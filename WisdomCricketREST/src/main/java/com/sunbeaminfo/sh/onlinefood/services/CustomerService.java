package com.sunbeaminfo.sh.onlinefood.services;

import com.sunbeaminfo.sh.onlinefood.entities.Customer;

public interface CustomerService {
	Customer getCustomerById(int id);
	Customer getCustomerByEmail(String email);
	void saveCustomer(Customer customer);
	Customer getCustomerByEmailAndPassword(String email, String password);
}
