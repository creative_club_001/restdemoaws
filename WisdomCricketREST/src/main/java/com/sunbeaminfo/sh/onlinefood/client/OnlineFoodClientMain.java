package com.sunbeaminfo.sh.onlinefood.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.client.RestTemplate;

import com.sunbeaminfo.sh.onlinefood.entities.Customer;

public class OnlineFoodClientMain {
	public static final String BASE_URL = "http://localhost:8080/sh11rest/";
	public static void main(String[] args) {
		AnnotationConfigApplicationContext ctx = 
				new AnnotationConfigApplicationContext(ClientConfig.class);
		RestTemplate restTemplate = ctx.getBean(RestTemplate.class);
		/*
		Customer cust = restTemplate.getForObject(BASE_URL+"customers/4", Customer.class);
		System.out.println(cust);
		*/
		
		Customer cust = new Customer(0, "Abc", "Pqr@123456", "xyz@gmail.com", "1234123412", "Temp, World");
		Customer insCust = restTemplate.postForObject(BASE_URL+"customers", cust, Customer.class);
		System.out.println(insCust);
		
		ctx.close();
	}
}
