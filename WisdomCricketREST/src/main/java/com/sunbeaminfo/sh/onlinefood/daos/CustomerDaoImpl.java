package com.sunbeaminfo.sh.onlinefood.daos;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sunbeaminfo.sh.onlinefood.entities.Customer;

@Repository
public class CustomerDaoImpl implements CustomerDao {
	@Autowired
	private SessionFactory factory;

	@Override
	public Customer getCustomerById(int id) {
		Session session = factory.getCurrentSession();
		return session.get(Customer.class, id);
	}

	@Override
	public Customer getCustomerByEmail(String email) {
		Session session = factory.getCurrentSession();
		String hql = "from Customers c where c.email=:p_email";
		Query<Customer> q = session.createQuery(hql);
		q.setParameter("p_email", email);
		return q.getSingleResult();
	}

	@Override
	public Customer getCustomerByMobile(String mobile) {
		Session session = factory.getCurrentSession();
		String hql = "from Customers c where c.mobile=:p_mobile";
		Query<Customer> q = session.createQuery(hql);
		q.setParameter("p_mobile", mobile);
		return q.getSingleResult();
	}

	@Override
	public void saveCustomer(Customer cust) {
		Session session = factory.getCurrentSession();
		session.save(cust);
	}
}
